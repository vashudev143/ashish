import React from "react";
import ExpenseForm from "./ExpenseForm";
const ExpenseForm2 = (props) => {
  const saveData = (data) => {
    const handleState = {
      ...data,
      id: Math.random().toString(),
    };
    return props.onAddEvent(handleState);
  };
  return (
    <div>
      <ExpenseForm onSaveData={saveData} />
    </div>
  );
};

export default ExpenseForm2;
