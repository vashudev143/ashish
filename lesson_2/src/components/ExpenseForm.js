import React, { useState } from "react";

const ExpenseForm = (props) => {
  const [addExpense, setAddExpense] = useState(true);
  const [input, setInput] = useState("");
  const [amount, setAmount] = useState("");
  const [date, setDate] = useState("");
  const inputHandle = (event) => {
    setInput(event.target.value);
  };
  const amountHandle = (event) => {
    setAmount(event.target.value);
  };
  const dateHandle = (event) => {
    setDate(event.target.value);
  };
  const submitHandle = (event) => {
    event.preventDefault();
    const handleState = {
      title: input,
      amount: amount,
      date: new Date(date),
    };
    setInput("");
    setAmount("");
    setDate("");
    setAddExpense(!addExpense);
    return props.onSaveData(handleState);
  };
  const handleCancel = () => {
    setInput("");
    setAmount("");
    setDate("");
  };
  return (
    <div>
      <form onSubmit={submitHandle}>
        <div className="row align-items-center g-3">
          {addExpense ? (
            <div className="row align-items-center g-3">
              <div className="col-auto">
                <label>title</label>
                <input type="text" value={input} onChange={inputHandle} />
              </div>
              <div className="col-auto">
                <label>amount</label>
                <input
                  type="number"
                  min="0.01"
                  step="0.01"
                  value={amount}
                  onChange={amountHandle}
                />
              </div>
              <div className="col-auto">
                <div className="form-check">
                  <label>date</label>
                  <input
                    type="date"
                    min="2019-1-1"
                    max="2022-12-31"
                    value={date}
                    onChange={dateHandle}
                  />
                </div>
              </div>
            </div>
          ) : (
            ""
          )}

          <div className="col-auto">
            <button type="submit" className="btn btn-primary">
              Add New Expense
            </button>
          </div>
        </div>
      </form>
      <button onClick={() => handleCancel()} className="btn btn-primary">
        Cancel
      </button>
    </div>
  );
};

export default ExpenseForm;
