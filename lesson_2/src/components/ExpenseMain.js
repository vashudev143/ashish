import React from "react";
import "./ExpenseMain.css";
const ExpenseMain = (props) => {
  const month = props.date.toLocaleString("en-us", { month: "long" });
  const day = props.date.toLocaleString("en-us", { day: "2-digit" });
  const year = props.date.getFullYear();
  console.log(props);
  // const [btnValue, setBtnValue] = useState(props.title);
  // const changeHandler = () => {
  //   setBtnValue("updatd");
  // };
  return (
    <>
      <div className="main">
        <div className="main-row">
          <div className="date">
            <div>{month}</div>
            <div>{year}</div>
            <div>{day}</div>
            {/* {props.date.toLocaleString()} */}
          </div>
          <div className="car">{props.title}</div>
        </div>
        <div className="amount">${props.amount}</div>
        {/* <div className="btn">
          <button onClick={changeHandler}>click me</button>
        </div> */}
      </div>
    </>
  );
};

export default ExpenseMain;

// note:- ExpenseMain ka data maine ExpenseMainData me bheja hai
