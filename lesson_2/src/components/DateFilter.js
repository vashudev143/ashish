import React from "react";

const DateFilter = (props) => {
  const send = (data) => {
    props.onDateHandle(data.target.value);
  };
  return (
    <div>
      <select
        className="form-select"
        aria-label="Default select example"
        defaultValue="Open this select menu"
        onChange={(data) => send(data)}
      >
        <option>Open this select menu</option>
        <option value="2022">2022</option>
        <option value="2021">2021</option>
        <option value="2020">2020</option>
        <option value="2019">2019</option>
        <option value="2018">2018</option>
      </select>
    </div>
  );
};

export default DateFilter;
// note:-iska datamaineExpenseMainData me bhej hai
