import React, { useState } from "react";
import DateFilter from "./DateFilter";
import ExpenseMain from "./ExpenseMain";
const ExpenseMainData = (props) => {
  //DataFilter start
  const [filteredYear, setFilteredYear] = useState("2019");
  const dateHandle = (selectedYear) => {
    setFilteredYear(selectedYear);
    //Data Filter End
  };
  const checkFilter = props.item.filter((data) => {
    return data.date.getFullYear().toString() === filteredYear;
  });
  console.log(checkFilter);
  return (
    <>
      <DateFilter onDateHandle={dateHandle} value={checkFilter} />
      {checkFilter.length === 0
        ? "data not found"
        : checkFilter.map((Expense, index) => {
            return (
              <ExpenseMain
                title={Expense.title}
                amount={Expense.amount}
                date={Expense.date}
                key={index}
              />
            );
          })}
    </>
  );
};

export default ExpenseMainData;
// note:- ExpenseMainData ka data App.js me import kara raha hu
