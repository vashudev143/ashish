import React, { useState } from "react";
import ExpenseMainData from "./components/ExpenseMainData";
import ExpenseForm2 from "./components/ExpenseForm2";
const ExpenseMainArray = [
  {
    id: 1,
    title: "car insurancce",
    amount: "48",
    date: new Date(2019, 11, 3),
  },
];
const App = () => {
  const [expenseData, setExpenseData] = useState(ExpenseMainArray);
  const addEvent = (data) => {
    setExpenseData((prevExpense) => {
      return [data, ...prevExpense];
    });
  };
  return (
    <div>
      <ExpenseForm2 onAddEvent={addEvent} />

      <ExpenseMainData item={expenseData} />
    </div>
  );
};

export default App;
// note:-maine ekk ExpenseMain mee ek array ke andar object banayaa aur ussh object ko ExpenseMainData me bulaunga
